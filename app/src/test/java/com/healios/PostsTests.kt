package com.healios

import androidx.lifecycle.ViewModelProvider
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.model.post.Post
import com.healios.presenter.PostPresenter
import com.healios.repository.PostDbRepository
import com.healios.repository.PostDbRepositoryImpl
import com.healios.repository.RepositoryCallBack
import com.healios.viewModel.PostViewModel
import com.nhaarman.mockito_kotlin.*
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import javax.inject.Inject

class PostsTests {
    private lateinit var postViewModel: PostViewModel
    private lateinit var retrofit: Retrofit
    private lateinit var db: HealiosDb
    private lateinit var view: PostPresenter
    private lateinit var postDbRepository: PostDbRepository

    @Before
    fun setup(){
        retrofit = mock()
        db = mock()
        view = mock()

        postViewModel = PostViewModel()
        postViewModel.init(retrofit, db, view)
        postDbRepository = mock()
    }

    @Test
    fun posts_getPosts_callisShowProgress(){
        postViewModel.getAllPosts()
        verify(view).showProgress()
    }
}