package com.healios.repository

import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User

interface PostRepository {
    fun getPostsData(callBack: RepositoryCallBack<MutableList<Post>>)
    fun getUserData(callBack: RepositoryCallBack<MutableList<User>>)
    fun getCommentData(callBack: RepositoryCallBack<MutableList<Comment>>)
}