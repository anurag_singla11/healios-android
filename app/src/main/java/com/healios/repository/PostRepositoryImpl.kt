package com.healios.repository

import com.google.gson.Gson
import com.healios.di.Api
import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User
import com.healios.utills.MyLog
import com.healios.viewModel.PostViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class PostRepositoryImpl(private val retrofit: Retrofit) : PostRepository {

    override fun getPostsData(callBack: RepositoryCallBack<MutableList<Post>>) {
        val apiService: Api = retrofit.create(Api::class.java)
        val response: Call<MutableList<Post>> =
            apiService.getAllPost()
        response.enqueue(object : Callback<MutableList<Post>?> {
            override fun onResponse(
                call: Call<MutableList<Post>?>,
                response: Response<MutableList<Post>?>
            ) {
                if (response.isSuccessful) {
                    val list = response.body()
                    if (!list.isNullOrEmpty()) {
                        MyLog.debug(
                            "Post:" + Gson().toJson(list),
                            PostViewModel::class.java
                        )
                        callBack.onSuccess(list)
                    }

                } else {
                    callBack.onFailure()
                }
            }

            override fun onFailure(call: Call<MutableList<Post>?>, t: Throwable) {
                MyLog.printException(t)
                callBack.onFailure()
            }

        })
    }

    override fun getUserData(callBack: RepositoryCallBack<MutableList<User>>) {
        val apiService: Api = retrofit.create(Api::class.java)
        val response: Call<MutableList<User>> =
            apiService.getAllUsers()
        response.enqueue(object : Callback<MutableList<User>?> {
            override fun onResponse(
                call: Call<MutableList<User>?>,
                response: Response<MutableList<User>?>
            ) {
                if (response.isSuccessful) {
                    val list = response.body()
                    if (!list.isNullOrEmpty()) {
                        MyLog.debug(
                            "User:" + Gson().toJson(list),
                            PostViewModel::class.java
                        )
                        callBack.onSuccess(list)
                    }

                } else {
                    callBack.onFailure()
                }
            }

            override fun onFailure(call: Call<MutableList<User>?>, t: Throwable) {
                MyLog.printException(t)
                callBack.onFailure()
            }

        })
    }

    override fun getCommentData(callBack: RepositoryCallBack<MutableList<Comment>>) {
        val apiService: Api = retrofit.create(Api::class.java)
        val response: Call<MutableList<Comment>> =
            apiService.getAllComment()
        response.enqueue(object : Callback<MutableList<Comment>?> {
            override fun onResponse(
                call: Call<MutableList<Comment>?>,
                response: Response<MutableList<Comment>?>
            ) {
                if (response.isSuccessful) {
                    val list = response.body()
                    if (!list.isNullOrEmpty()) {
                        MyLog.debug(
                            "Comment:" + Gson().toJson(list),
                            PostViewModel::class.java
                        )
                        callBack.onSuccess(list)
                    }

                } else {
                    callBack.onFailure()
                }
            }

            override fun onFailure(call: Call<MutableList<Comment>?>, t: Throwable) {
                MyLog.printException(t)
                callBack.onFailure()
            }

        })
    }
}
