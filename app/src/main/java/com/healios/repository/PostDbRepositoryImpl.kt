package com.healios.repository

import com.agnity.aconyx.defend.db.HealiosDb
import com.agnity.aconyx.defend.db.HealiosDb_Impl
import com.healios.model.post.Post
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PostDbRepositoryImpl(private val healiosdb: HealiosDb) : PostDbRepository {

    override fun getAllPosts(repositoryCallBack: RepositoryCallBack<MutableList<Post>?>) {
        GlobalScope.launch(Dispatchers.IO) {
            val posts = healiosdb.postDao().getAllPost()
            if (!posts.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(posts)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }

    override fun insertAllPosts(posts: MutableList<Post>) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosdb.postDao().insertAll(posts)
        }
    }

    override fun insertAllPosts(
        posts: MutableList<Post>,
        repositoryCallBack: RepositoryCallBack<MutableList<Post>?>) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosdb.postDao().insertAll(posts)
            val postsList = healiosdb.postDao().getAllPost()
            if (!postsList.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(postsList)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }


    override fun getCount(): Int {
        return healiosdb.postDao().getCount()
    }

}