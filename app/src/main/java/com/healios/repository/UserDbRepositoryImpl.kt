package com.healios.repository

import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.model.userInfo.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class UserDbRepositoryImpl(private val healiosDb: HealiosDb) : UserDbRepository {
    override fun getAllUsers(repositoryCallBack: RepositoryCallBack<MutableList<User>?>) {
        GlobalScope.launch(Dispatchers.IO) {
            val users = healiosDb.userDao().getAllUsers()
            if (!users.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(users)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }

    override fun insertAllUsers(users: MutableList<User>) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosDb.userDao().insertAll(users)
        }
    }

    override fun insertAllUsers(
        users: MutableList<User>,
        repositoryCallBack: RepositoryCallBack<MutableList<User>?>
    ) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosDb.userDao().insertAll(users)
            val savedUsers = healiosDb.userDao().getAllUsers()
            if (!savedUsers.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(savedUsers)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }

    override fun getUserDetails(
        userId: Int,
        repositoryCallBack: RepositoryCallBack<User?>
    ){
        GlobalScope.launch(Dispatchers.IO) {
            val user = healiosDb.userDao().getUserDetails(userId)
            repositoryCallBack.onSuccess(user)
        }
    }
}