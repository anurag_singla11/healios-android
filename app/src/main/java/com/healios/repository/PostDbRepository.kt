package com.healios.repository

import com.healios.model.post.Post

interface PostDbRepository {
    fun getAllPosts(repositoryCallBack: RepositoryCallBack<MutableList<Post>?>)
    fun insertAllPosts(posts: MutableList<Post>)
    fun insertAllPosts(posts: MutableList<Post>, repositoryCallBack: RepositoryCallBack<MutableList<Post>?>)
    fun getCount(): Int
}