package com.healios.repository

import com.healios.model.comments.Comment
import com.healios.model.userInfo.User

interface CommentsDbRepository {
    fun getAllComments(repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>)
    fun insertAllComments(comments: MutableList<Comment>)
    fun insertAllComments(comments: MutableList<Comment>, repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>)
    fun getPostComments(id: Int, repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>)
}