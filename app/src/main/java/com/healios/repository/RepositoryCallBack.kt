package com.healios.repository

interface RepositoryCallBack<in T>{
    fun onSuccess(t:T?)
    fun onFailure()
}