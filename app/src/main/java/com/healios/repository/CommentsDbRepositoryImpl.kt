package com.healios.repository

import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.model.comments.Comment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CommentsDbRepositoryImpl(private val healiosDb: HealiosDb) : CommentsDbRepository {

    override fun getAllComments(repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>) {
        GlobalScope.launch(Dispatchers.IO) {
            val comments = healiosDb.commentsDao().getAllComments()
            if (!comments.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(comments)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }

    override fun insertAllComments(comments: MutableList<Comment>) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosDb.commentsDao().insertAll(comments)
        }
    }

    override fun insertAllComments(
        comments: MutableList<Comment>,
        repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>
    ) {
        GlobalScope.launch(Dispatchers.IO) {
            healiosDb.commentsDao().insertAll(comments)
            val savedComments = healiosDb.commentsDao().getAllComments()
            if (!savedComments.isNullOrEmpty()) {
                repositoryCallBack.onSuccess(savedComments)
            } else {
                repositoryCallBack.onFailure()
            }
        }
    }

    override fun getPostComments(
        id: Int,
        repositoryCallBack: RepositoryCallBack<MutableList<Comment>?>
    ) {
        GlobalScope.launch(Dispatchers.IO) {
            val comments = healiosDb.commentsDao().getPostComments(id)
            repositoryCallBack.onSuccess(comments)
        }
    }
}