package com.healios.repository

import com.healios.model.userInfo.User

interface UserDbRepository {
    fun getAllUsers(repositoryCallBack: RepositoryCallBack<MutableList<User>?>)
    fun insertAllUsers(users: MutableList<User>)
    fun insertAllUsers(users: MutableList<User>, repositoryCallBack: RepositoryCallBack<MutableList<User>?>)
    fun getUserDetails(userId: Int, repositoryCallBack: RepositoryCallBack<User?>)
}