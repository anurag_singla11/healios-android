package com.agnity.aconyx.defend.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.healios.db.CommentsDao
import com.healios.db.PostDao
import com.healios.db.UserDao
import com.healios.model.post.Post
import com.healios.model.userInfo.User


@Database(entities = [Post::class, User::class,com.healios.model.comments.Comment::class], version = 1)
abstract class HealiosDb : RoomDatabase() {
    abstract fun postDao(): PostDao
    abstract fun userDao(): UserDao
    abstract fun commentsDao(): CommentsDao

}