package com.healios.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.healios.model.post.Post

@Dao
interface PostDao {
    @Query("SELECT * FROM post_table ")
    suspend fun getAllPost(): MutableList<Post>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list:MutableList<Post>)

    @Query("SELECT COUNT(id) FROM post_table ")
    fun getCount(): Int
}