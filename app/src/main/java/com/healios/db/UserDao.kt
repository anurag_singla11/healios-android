package com.healios.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.healios.model.userInfo.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user_table ")
    suspend fun getAllUsers(): MutableList<User>?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: MutableList<User>)

    @Query("SELECT * from user_table where id=:id ")
    suspend fun getUserDetails(id: Int): User?
}