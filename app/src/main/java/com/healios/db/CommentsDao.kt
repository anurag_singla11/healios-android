package com.healios.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.healios.model.comments.Comment

@Dao
interface CommentsDao {
    @Query("SELECT * FROM comments_table ")
    suspend fun getAllComments(): MutableList<Comment>?
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(list: MutableList<Comment>)
    @Query("SELECT * from comments_table where postId=:id ")
    suspend fun getPostComments(id: Int): MutableList<Comment>?
}