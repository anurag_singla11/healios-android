package com.healios.utills

import android.util.Log

object MyLog {
    var LOG_TAG = "Healios"
    fun error(msg: String, aClass: Class<*>) {
        Log.e(LOG_TAG, "-->> " + aClass.simpleName + " : " + msg)
    }

    fun debug(msg: String, aClass: Class<*>) {
        Log.d(LOG_TAG, "-->> " + aClass.simpleName + " : " + msg)
    }

    fun printException(e: Throwable) {
        Exception(e).printStackTrace()
    }
}