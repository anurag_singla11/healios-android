package com.healios.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.MyApplication
import com.healios.databinding.PostFragmentBinding
import com.healios.model.post.Post
import com.healios.presenter.PostPresenter
import com.healios.viewModel.PostViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import javax.inject.Inject

class PostFragment : Fragment(), PostPresenter {

    companion object {
        fun newInstance() = PostFragment()
    }

    private lateinit var viewModel: PostViewModel
    private lateinit var binding: PostFragmentBinding

    @Inject
    lateinit var retrofit: Retrofit

    @Inject
    lateinit var db: HealiosDb

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        MyApplication.netComponent!!.inject(this)
        binding = PostFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUI()
    }

    fun initUI(){
        binding.postRecyclerviewPosts.layoutManager =
            LinearLayoutManager(binding.root.context, RecyclerView.VERTICAL, false)
        viewModel = ViewModelProvider(this).get(PostViewModel::class.java)
        viewModel.init(retrofit, db, this)
        viewModel.getAllPosts()
    }

    override fun showProgress() {
        GlobalScope.launch(Dispatchers.Main) {
            binding.postProgressCircular.visibility = View.VISIBLE
        }
    }

    override fun hideProgress() {
        GlobalScope.launch(Dispatchers.Main) {
            binding.postProgressCircular.visibility = View.GONE
        }
    }

    override fun showError(message: String) {
        GlobalScope.launch(Dispatchers.Main) {
            Toast.makeText(binding.root.context, message, Toast.LENGTH_LONG).show()
        }
    }

    override fun showPosts(list: MutableList<Post>?) {
        GlobalScope.launch(Dispatchers.Main) {
            if (list != null) {
                binding.postRecyclerviewPosts.adapter =
                    PostAdapter(list, binding.root.context, object : OnRowClickListener {
                        override fun onClick(post: Post) {
                            UserDetailsActivity.openActivity(binding.root.context, post)
                        }
                    })
            }
        }
    }

    override fun showEmptyView() {
        Toast.makeText(binding.root.context, "No Data Found!", Toast.LENGTH_LONG).show()
    }

}