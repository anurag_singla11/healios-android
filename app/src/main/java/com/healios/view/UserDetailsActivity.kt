package com.healios.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.healios.R
import com.healios.databinding.ActivityMainBinding
import com.healios.model.post.Post

class UserDetailsActivity : AppCompatActivity() {
    companion object{
        const val POST_DETAILS_KEY = "post";
        fun openActivity(context: Context,post:Post){
            val intent= Intent(context,UserDetailsActivity::class.java).apply {
                putExtra(POST_DETAILS_KEY , post)
            }
            context.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        val view: View = binding.getRoot()
        setContentView(view)
        initUI()
    }

    fun initUI(){
        val fragment = PostDetailsFragment.newInstance()
        fragment.arguments = intent.extras
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitNow()
    }
}