package com.healios.view

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.healios.databinding.ListRowPostBinding
import com.healios.model.post.Post

class PostAdapter(
    var list: MutableList<Post>,
    var context: Context,
    var listener: OnRowClickListener
) :
    RecyclerView.Adapter<PostAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListRowPostBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (list.isNotEmpty()) {
            val callDetail = list[position]
            holder.bind(callDetail)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder internal constructor(private var binding: ListRowPostBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(post: Post) {
            binding.title.text=post.title
            binding.desc.text=post.body
            ( binding.root as View).setOnClickListener {
                listener.onClick(post)
            }
        }

    }
}

interface OnRowClickListener {
    fun onClick(post:Post)
}
