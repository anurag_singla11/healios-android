package com.healios.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.MyApplication
import com.healios.R
import com.healios.databinding.PostDetalisFragmentBinding
import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User
import com.healios.presenter.DetailsPresenter
import com.healios.viewModel.PostDetailsViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class PostDetailsFragment : Fragment(), DetailsPresenter {

    companion object {
        fun newInstance() = PostDetailsFragment()
    }

    private lateinit var viewModel: PostDetailsViewModel
    private lateinit var binding: PostDetalisFragmentBinding

    @Inject
    lateinit var db: HealiosDb

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        MyApplication.netComponent!!.inject(this)
        binding = PostDetalisFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.postDetialsRecyclerviewComments.layoutManager =
            LinearLayoutManager(binding.root.context, RecyclerView.VERTICAL, false)
        viewModel = ViewModelProvider(this).get(PostDetailsViewModel::class.java)
        viewModel.init(db, this)
        val post = arguments?.getParcelable<Post>(UserDetailsActivity.POST_DETAILS_KEY)
        binding.postDetailsTitle.text = post?.title
        binding.postDetialsDesc.text = post?.body
        viewModel.getUserDetails(post!!)
    }

    override fun showProgress() {
        GlobalScope.launch(Dispatchers.Main) {
            binding.postDetailsProgressCircular.visibility = View.VISIBLE
        }
    }

    override fun hideProgress() {
        GlobalScope.launch(Dispatchers.Main) {
            binding.postDetailsProgressCircular.visibility = View.GONE
        }
    }

    override fun showError(message: String) {
        GlobalScope.launch(Dispatchers.Main) {
            Toast.makeText(binding.root.context, "Something went wrong", Toast.LENGTH_LONG).show()
        }
    }

    override fun showComments(list: MutableList<Comment>?) {
        GlobalScope.launch(Dispatchers.Main) {
            if (list != null) {
                binding.postDetialsRecyclerviewComments.adapter =
                    CommentsAdapter(list, binding.root.context)
            }
        }
    }

    override fun showUserDetails(user: User?) {
        user?.let {
            binding.postDetailsUsername.text = getString(R.string.post_details_username, user.name)
            binding.postDetailsEmail.text =  getString(R.string.post_details_useremail, user.email)
            binding.postDetailsAddress.text = getString(R.string.post_details_useraddress, user.address?.street + ", " + user.address?.city)
            binding.postDetailsCompany.text = getString(R.string.post_details_usercompany, user.company?.name)
        }
    }
}