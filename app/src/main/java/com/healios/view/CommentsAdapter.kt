package com.healios.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.healios.databinding.ListRowPostBinding
import com.healios.model.comments.Comment

class CommentsAdapter(
    var list: MutableList<Comment>,
    var context: Context
) :
    RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ListRowPostBinding
            .inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (list.isNotEmpty()) {
            val callDetail = list[position]
            holder.bind(callDetail)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }


    inner class ViewHolder internal constructor(private var binding: ListRowPostBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(comment: Comment) {
            binding.title.text = comment.name
            binding.desc.text = comment.body
        }

    }
}

