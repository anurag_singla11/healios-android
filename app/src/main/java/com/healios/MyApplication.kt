package com.healios

import android.app.Application
import com.healios.di.*

class MyApplication : Application() {
    companion object {
         var netComponent:ApiComponent?=null
         var app:MyApplication?=null
    }


    override fun onCreate() {
        super.onCreate()
        netComponent = DaggerApiComponent.builder()
            .appModule(AppModule(this))
            .apiModule(ApiModule(Api.BASE_URL))
            .dataBaseModule(DataBaseModule(this)).build()
        app = this
    }
}