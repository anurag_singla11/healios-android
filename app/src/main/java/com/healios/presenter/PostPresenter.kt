package com.healios.presenter

import com.healios.model.post.Post

interface PostPresenter {
    fun showProgress()
    fun hideProgress()
    fun showError(message:String)
    fun showPosts(list: MutableList<Post>?)
    fun showEmptyView()
}