package com.healios.presenter

import com.healios.model.comments.Comment
import com.healios.model.userInfo.User

interface DetailsPresenter {
    fun showProgress()
    fun hideProgress()
    fun showError(message: String)
    fun showComments(list: MutableList<Comment>?)
    fun showUserDetails(user: User?)
}