package com.healios

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.healios.databinding.ActivityMainBinding
import com.healios.view.PostFragment

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        val view: View = binding.getRoot()
        setContentView(view)
        launchPostListFragment()
    }

    fun launchPostListFragment(){
        val fragment = PostFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, fragment)
            .commitNow()
    }
}