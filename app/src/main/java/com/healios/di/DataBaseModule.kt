package com.healios.di

import android.app.Application
import androidx.room.Room
import com.agnity.aconyx.defend.db.HealiosDb
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class DataBaseModule(var application: Application) {
    @Provides
    @Singleton
    fun provideHealiosDb(): HealiosDb {
        return   Room.databaseBuilder(
            application,
            HealiosDb::class.java,
            "healios_db"
        ).build()
    }
}