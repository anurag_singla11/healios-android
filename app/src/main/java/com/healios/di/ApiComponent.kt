package com.healios.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.healios.view.PostDetailsFragment
import com.healios.view.PostFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class,DataBaseModule::class])
interface ApiComponent {
    fun inject(activity: AppCompatActivity?)
    fun inject(fragment: PostFragment?)
    fun inject(fragment: PostDetailsFragment?)
    fun inject(viewModel: ViewModel)
}