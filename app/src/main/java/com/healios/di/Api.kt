package com.healios.di

import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User
import retrofit2.Call
import retrofit2.http.GET

interface Api {

    companion object {
        const val BASE_URL = "http://jsonplaceholder.typicode.com/"
        const val POSTS = "posts"
        const val USERS = "users"
        const val COMMENTS = "comments"
    }

    @GET(POSTS)
    fun getAllPost(): Call<MutableList<Post>>

    @GET(USERS)
    fun getAllUsers(): Call<MutableList<User>>

    @GET(COMMENTS)
    fun getAllComment(): Call<MutableList<Comment>>
}