package com.healios.viewModel

import androidx.lifecycle.ViewModel
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User
import com.healios.presenter.DetailsPresenter
import com.healios.repository.CommentsDbRepositoryImpl
import com.healios.repository.RepositoryCallBack
import com.healios.repository.UserDbRepositoryImpl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class PostDetailsViewModel : ViewModel() {
    fun init(db: HealiosDb, presenter: DetailsPresenter) {
        this.presenter = presenter
        this.db = db
    }

    lateinit var presenter: DetailsPresenter
    lateinit var db: HealiosDb
     fun getUserDetails(post: Post) {
         presenter.showProgress()
       GlobalScope.launch(Dispatchers.IO) {
           UserDbRepositoryImpl(db).getUserDetails(post.userId!!, object : RepositoryCallBack<User?>{
               override fun onSuccess(t: User?) {
                   presenter.showUserDetails(t)
               }

               override fun onFailure() {}

           })

           CommentsDbRepositoryImpl(db).getPostComments(post.id, object: RepositoryCallBack<MutableList<Comment>?>{
               override fun onSuccess(t: MutableList<Comment>?) {
                   presenter.showComments(t)
               }

               override fun onFailure() {}
           })

           presenter.hideProgress()
       }
    }

}