package com.healios.viewModel

import androidx.lifecycle.ViewModel
import com.agnity.aconyx.defend.db.HealiosDb
import com.google.gson.Gson
import com.healios.model.comments.Comment
import com.healios.model.post.Post
import com.healios.model.userInfo.User
import com.healios.presenter.PostPresenter
import com.healios.repository.*
import com.healios.utills.MyLog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit

class PostViewModel : ViewModel() {
    fun init(retrofit: Retrofit, db: HealiosDb, presenter: PostPresenter) {
        this.retrofit = retrofit
        this.view = presenter
        this.db = db
        postRepository = PostRepositoryImpl(retrofit)
    }

    lateinit var retrofit: Retrofit
    lateinit var db: HealiosDb
    lateinit var view: PostPresenter
    lateinit var postRepository: PostRepository

    fun getAllPosts() {
        GlobalScope.launch(Dispatchers.IO) {
            view.showProgress()

            val postDbRepository = PostDbRepositoryImpl(db)
            postDbRepository.getAllPosts(object : RepositoryCallBack<MutableList<Post>?>{
                override fun onSuccess(t: MutableList<Post>?) {
                    view.showPosts(t)
                    view.hideProgress()
                }

                override fun onFailure() {
                    postRepository.getPostsData(object : RepositoryCallBack<MutableList<Post>> {
                        override fun onSuccess(t: MutableList<Post>?) {
                            if (!t.isNullOrEmpty()) {
                                MyLog.debug(
                                    "Post:" + Gson().toJson(t),
                                    PostViewModel::class.java
                                )
                                postDbRepository.insertAllPosts(t)
                                view.showPosts(t)
                            }else{
                                view.showEmptyView()
                            }

                            getAllUsers()
                            getAllComments()
                            view.hideProgress()
                        }

                        override fun onFailure() {
                            view.hideProgress()
                            view.showError("Something went wrong, please try again later.")
                        }

                    })
                }

            })
        }
    }

    fun getAllUsers() {
        GlobalScope.launch(Dispatchers.IO) {
            postRepository.getUserData(object :RepositoryCallBack<MutableList<User>>{
                override fun onSuccess(t: MutableList<User>?) {
                    if (t != null) {
                        MyLog.debug("Users:" + Gson().toJson(t), PostViewModel::class.java)
                        val userDbRepository = UserDbRepositoryImpl(db)
                        userDbRepository.insertAllUsers(t)
                    }
                }

                override fun onFailure() {}

            })
        }
    }

    fun getAllComments() {
        GlobalScope.launch(Dispatchers.IO) {
            postRepository.getCommentData(object :RepositoryCallBack<MutableList<Comment>>{
                override fun onSuccess(t: MutableList<Comment>?) {
                    if (t != null) {
                        MyLog.debug("Comments:" + Gson().toJson(t), PostViewModel::class.java)
                        val commentsDbRepository = CommentsDbRepositoryImpl(db)
                        commentsDbRepository.insertAllComments(t)
                    }
                }

                override fun onFailure() {}

            })
        }
    }
}