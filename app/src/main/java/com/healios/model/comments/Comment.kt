package com.healios.model.comments

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "comments_table")
class Comment {
    @PrimaryKey
    var id: Int = 0
    var body: String? = null
    var email: String? = null
    var name: String? = null
    var postId: Int = 0
    constructor()
    constructor(id: Int, postId: Int, body: String,email: String,name: String){
        this.id=id
        this.body=body
        this.email=email
        this.name=name
        this.postId=postId
    }
}