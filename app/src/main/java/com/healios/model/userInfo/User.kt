package com.healios.model.userInfo

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.google.gson.Gson

@Entity(tableName = "user_table")
class User {
    @PrimaryKey
    var id: Int = 0
    @TypeConverters(AddressConverter::class)
    var address: Address? = null
    @TypeConverters(CompanyConverter::class)
    var company: Company? = null
    var email: String? = null
    var name: String? = null
    var phone: String? = null
    var username: String? = null
    var website: String? = null
    constructor()
    constructor(id: Int,email: String,name: String,username: String){
        this.id=id
        this.email=email
        this.name=name
        this.username=username
    }

    constructor(id: Int,email: String,name: String,username: String, address: Address, company: Company){
        this.id=id
        this.email=email
        this.name=name
        this.username=username
        this.address = address;
        this.company = company;
    }
}

class AddressConverter {
    var gson = Gson()

    @TypeConverter
    fun addressToString(foodItems: Address): String {
        return gson.toJson(foodItems)
    }

    @TypeConverter
    fun stringToAddress(data: String): Address {
        return gson.fromJson(data, Address::class.java)
    }
}
class CompanyConverter {
    var gson = Gson()

    @TypeConverter
    fun companyToString(foodItems: Company): String {
        return gson.toJson(foodItems)
    }

    @TypeConverter
    fun stringToCompany(data: String): Company {
        return gson.fromJson(data, Company::class.java)
    }
}
