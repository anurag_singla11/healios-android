package com.healios.model.post

import android.os.Parcel
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "post_table")
class Post() : Parcelable {
    var userId: Int = 0

    @PrimaryKey
    var id: Int = 0

    var body: String? = null
    var title: String? = null

    constructor(userId: Int, id: Int, title: String, body: String) : this() {
        this.userId = userId
        this.id = id
        this.title = title
        this.body = body

    }

    constructor(parcel: Parcel) : this() {
        userId = parcel.readInt()
        id = parcel.readInt()
        body = parcel.readString()
        title = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(userId)
        parcel.writeInt(id)
        parcel.writeString(body)
        parcel.writeString(title)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Post> {
        override fun createFromParcel(parcel: Parcel): Post {
            return Post(parcel)
        }

        override fun newArray(size: Int): Array<Post?> {
            return arrayOfNulls(size)
        }
    }


}