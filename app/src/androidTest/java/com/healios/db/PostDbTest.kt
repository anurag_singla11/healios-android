package com.healios

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.db.PostDao
import com.healios.model.post.Post
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class PostDbTest : TestCase() {
    private lateinit var db: HealiosDb
    private lateinit var postDao: PostDao

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, HealiosDb::class.java).build()
        postDao = db.postDao()
    }

    @After
    fun closeDb(){
        db.close()
    }

    @Test
    fun writeAndReadPosts() = runBlocking {
        val post = Post(1, 1, "Test title", "Test body")

        val postList = mutableListOf<Post>()
        postList.add(post)

        postDao.insertAll(postList)

        val posts = postDao.getAllPost()

        assertEquals(1, posts?.size)
        assertEquals(1, posts?.get(0)?.id)
        assertEquals("Test title", posts?.get(0)?.title)
        assertNotSame(1, posts?.size)
    }

}