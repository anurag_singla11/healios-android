package com.healios

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.db.CommentsDao
import com.healios.model.comments.Comment
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class CommentsDbTest : TestCase() {
    private lateinit var db: HealiosDb
    private lateinit var commentsDao: CommentsDao

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, HealiosDb::class.java).build()
        commentsDao = db.commentsDao()
    }

    @After
    fun closeDb(){
        db.close()
    }

    @Test
    fun writeAndReadComments() = runBlocking {
        val comment = Comment(1, 1, "Test body", "test@gmail.com", "Test name")

        val commentsList = mutableListOf<Comment>()
        commentsList.add(comment)

        commentsDao.insertAll(commentsList)

        val comments = commentsDao.getAllComments()

        assertEquals(1, comments?.size)
        assertEquals(1, comments?.get(0)?.id)
        assertNotSame("Test title", comments?.get(0)?.email)
        assertEquals("test@gmail.com", comments?.get(0)?.email)
        assertNotSame(1, comments?.size)
    }

}