package com.healios

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.agnity.aconyx.defend.db.HealiosDb
import com.healios.db.UserDao
import com.healios.model.userInfo.Address
import com.healios.model.userInfo.Company
import com.healios.model.userInfo.User
import junit.framework.TestCase
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class UserDbTest : TestCase() {
    private lateinit var db: HealiosDb
    private lateinit var userDao: UserDao

    @Before
    public override fun setUp() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(context, HealiosDb::class.java).build()
        userDao = db.userDao()
    }

    @After
    fun closeDb(){
        db.close()
    }

    @Test
    fun writeAndReadUsers() = runBlocking {
        val user1 = User(1, "test1@gmail.com", "test user1", "testuser1", Address(), Company())
        val user2 = User(2, "test2@gmail.com", "test user2", "testuser2", Address(), Company())

        val userList = mutableListOf<User>()
        userList.add(user1)

        userDao.insertAll(userList)

        val users = userDao.getAllUsers()

        assertEquals(1, users?.size)
        assertEquals(1, users?.get(0)?.id)
        assertEquals("test1@gmail.com", users?.get(0)?.email)
        assertNotSame(user2.id, users?.get(0)?.id)
    }

}